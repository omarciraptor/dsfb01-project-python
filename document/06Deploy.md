# DESPLIEGUE DE UN MODELO DE MACHINE LEARNING
<img src="../src/img/deply_image.png" width="1020" height="480" align="center">

## ¿Que es el despliegue de un modelo de machine learning?

El despliegue de un modelo es el método mediante el cual integra un modelo de **Machine Learning** en un entorno de producción existente 
para tomar decisiones comerciales prácticas basadas en datos. Es una de las últimas etapas del ciclo de vida del aprendizaje automático y puede ser una de las más engorrosas. 
## ¿Porqué es importante el despliegue de un modelo?

Para comenzar a utilizar un modelo para la toma de decisiones prácticas, debe implementarse efectivamente en la producción. Si no puede obtener información práctica confiable de su modelo, entonces el impacto del modelo está severamente limitado.

El despliegue del modelo es uno de los procesos más difíciles de obtener valor del aprendizaje automático. Requiere coordinación entre científicos de datos, equipos de TI, desarrolladores de software y profesionales de negocios para garantizar que el modelo funcione de manera confiable en el entorno de producción de la organización. Esto presenta un gran desafío porque a menudo existe una discrepancia entre el lenguaje de programación en el que se escribe un modelo de aprendizaje automático y los idiomas que su sistema de producción puede comprender, y la codificación del modelo puede extender la línea de tiempo del proyecto por semanas o meses.

Para obtener el máximo valor de los modelos de aprendizaje automático, es importante implementarlos sin problemas en la producción para que una empresa pueda comenzar a usarlos para tomar decisiones prácticas

# TIPOS DE DESPLIEGUES DE MODELOS SEGUN EL ENTRENAMIENTO

## 1. Batch Training
Si bien no es completamente necesario implementar un modelo en producción en este caso, el entrenamiento por batch permite tener una versión constantemente actualizada de su modelo basada en el último tren.
El entrenamiento por lotes puede beneficiar mucho de los frameworks tipo AutoML, AutoML le permite realizar / automatizar actividades como el procesamiento de características, la selección de características, las selecciones de modelos y la optimización de parámetros. Su desempeño reciente ha estado a la par o ha superado a los científicos de datos más diligentes.

## 2. Real-time training 
Si bien la inferencia por lotes es más simple que la inferencia en línea, esta simplicidad presenta desafíos. Obviamente, las predicciones generadas en lote no están disponibles para fines en tiempo real. Esto significa que las predicciones pueden no estar disponibles para nuevos datos.El inconveniente de este enfoque es que hay más modelos para construir, implementar, monitorear, etc.

# Batch v Real time Training
Cuando se analiza si se debe hacer el despliegue del modelo por batch o en real time, una de las pregustas mas importantes es si es que es indispensable realizar predicciones en tiempo real.

+ **Implicaciones de infraestructura**: 

    Yendo en tiempo real, pone una responsabilidad operativa mucho más alta. Las personas deben ser capaces de monitorear cómo funciona el sistema, recibir alertas cuando hay un problema y tomar alguna consideración con respecto a la responsabilidad de conmutación por error. Para la predicción por lotes, la obligación operativa es mucho menor, definitivamente se necesita cierto monitoreo y se desea alterar, pero la necesidad de poder conocer los problemas que surgen directamente es mucho menor.

+ **Implicaciones de costos**:

    Ir a predicciones en tiempo real también tiene implicaciones de costos, ir por más potencia de cómputo, no poder distribuir la carga durante el día puede forzar la compra de más capacidad de cómputo de la que necesitaría o pagar el aumento de precio spot. Dependiendo del enfoque y los requisitos adoptados, también podría haber un costo adicional debido a la necesidad de una capacidad de cómputo más potente para cumplir con los SLA. Además, tenderá a haber una mayor huella de infraestructura al elegir predicciones en tiempo real. Una advertencia potencial allí es donde se toma la decisión de confiar en la predicción de la aplicación, para ese escenario específico, el costo podría terminar siendo más barato que optar por un enfoque por lotes.

+ **Implicaciones de evaluación**:

    Evaluar el rendimiento de la predicción en tiempo real puede ser más difícil que para las predicciones por batch. ¿Cómo evalúa el rendimiento cuando se enfrenta a una sucesión de acciones en un breve estallido que produce múltiples predicciones para un cliente determinado, por ejemplo? Evaluar y depurar modelos de predicción en tiempo real es significativamente más complejo de administrar. También requieren un mecanismo de recopilación de registros que permita recopilar las diferentes predicciones y características que produjeron el puntaje para una evaluación adicional.

## EJEMPLOS:
### Batch training

Suponga que su empresa ha creado un modelo de puntuación líder para predecir si los nuevos clientes potenciales comprarán su producto o servicio. El equipo de marketing pide que se califiquen nuevos clientes potenciales dentro de las 24 horas posteriores a la entrada al sistema. Podemos realizar inferencias cada noche en el lote de clientes potenciales generados ese día, garantizando que los clientes potenciales se puntúen dentro del plazo acordado.

O considere las recomendaciones de productos en un sitio de comercio electrónico como Amazon. En lugar de generar nuevas predicciones cada vez que un usuario inicia sesión en Amazon, los científicos de datos pueden decidir generar recomendaciones para los usuarios en lotes y luego almacenarlas en caché para una fácil recuperación cuando sea necesario. Del mismo modo, si está desarrollando un servicio como Netflix donde recomienda a los espectadores una lista de películas, puede que no tenga sentido generar recomendaciones cada vez que un usuario inicia sesión. En su lugar, puede generar estas recomendaciones por lotes en algún horario recurrente.


<img src="../src/img/netflix.jpg" width="45%" align="center"/> <img src="../src/img/amazon.png" width="45%" align="center"/>

### Real time training

Básicamente, la inferencia en línea debe realizarse siempre que se requieran predicciones en tiempo real. Por ejemplo, se genera un tiempo estimado de entrega de UberEats cada vez que un usuario ordena comida a través del servicio. No sería posible generar un lote de estas estimaciones y luego servirlas a los usuarios. Imagínese no saber cuánto tardará en recibir su pedido hasta que llegue la comida. Otros ejemplos de aplicaciones que pueden beneficiarse de la inferencia en línea son la realidad aumentada, la realidad virtual, las interfaces hombre-computadora, los automóviles autónomos y cualquier aplicación orientada al consumidor que permita a los usuarios consultar modelos en tiempo real.

Mencionamos los sistemas de recomendación anteriormente como ejemplos donde se pueden generar inferencias en lotes. Dependiendo del caso de uso, las recomendaciones también se pueden servir en línea. Por ejemplo, si una aplicación web proporciona recomendaciones para nuevos destinos de viaje basadas en la entrada de formularios, se requiere inferencia en línea para servir las recomendaciones dentro de una respuesta web.

<img src="../src/img/ubereats.png" height="480" width="45%" align="center"/> <img src="../src/img/viajes.jpg" height="480" width="45%" align="center"/>